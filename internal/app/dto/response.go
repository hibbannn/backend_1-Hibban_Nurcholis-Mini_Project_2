package dto

import "time"

type Response struct {
	Success      bool   `json:"success"`
	MessageTitle string `json:"messageTitle"`
	Message      string `json:"message"`
	ResponseTime string `json:"responseTime"`
}

type ActorResponse struct {
	Id        uint      `json:"id"`
	RoleId    uint      `json:"role_id"`
	Verified  bool      `json:"verified"`
	Active    bool      `json:"active"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

func NewResponse(success bool, messageTitle string, message string, responseTime string) *Response {
	return &Response{
		Success:      success,
		MessageTitle: messageTitle,
		Message:      message,
		ResponseTime: responseTime,
	}
}

type SuccessCreateActor struct {
	Response
	Data ActorResponse `json:"data"`
}
