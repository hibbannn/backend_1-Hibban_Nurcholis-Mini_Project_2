package dto

type ErrorResponse struct {
	Response
	Data   interface{} `json:"data,omitempty"`
	Errors interface{} `json:"errors,omitempty"`
}

func NewErrorResponse(success bool, messageTitle string, message string, responseTime string, data interface{}, errors interface{}) *ErrorResponse {
	return &ErrorResponse{
		Response: Response{
			Success:      success,
			MessageTitle: messageTitle,
			Message:      message,
			ResponseTime: responseTime,
		},
		Data:   data,
		Errors: errors,
	}
}

func NewDefaultErrorResponse() *ErrorResponse {
	return NewErrorResponse(false, "Oops, something went wrong.", "", "", nil, nil)
}

func NewBadRequestErrorResponse(message string) *ErrorResponse {
	return NewErrorResponse(false, "Oops, something went wrong.", message, "", nil, nil)
}

func NewInvalidDataErrorResponse(data interface{}) *ErrorResponse {
	return NewErrorResponse(false, "Oops, something went wrong.", "Invalid form data.", "", data, nil)
}

func NewInvalidResponse(errors interface{}) *ErrorResponse {
	return NewErrorResponse(false, "Oops, something went wrong.", "Invalid data.", "", nil, errors)
}
