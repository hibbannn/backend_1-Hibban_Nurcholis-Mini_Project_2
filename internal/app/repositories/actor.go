package repositories

import (
	"gitlab.com/hibbannn/backend_1-Hibban_Nurcholis-Mini_Project_2/internal/app/modules/actor/models"
	"gorm.io/gorm"
)

type ActorRepository interface {
	GetAll() ([]*models.Actor, error)
	RegisterActor(*models.Actor) (*models.Actor, error)
	RegisterApproval(*models.Approval) (*models.Approval, error)
	GetAllApprovalRequest() ([]*models.Approval, error)
	GetAdminById(adminId uint64) (models.Actor, error)
	UpdateAdmin(*models.Actor) (*models.Actor, error)
	GetApprovalByAdminId(adminId uint64) (models.Approval, error)
	UpdateApproval(*models.Approval) (*models.Approval, error)
	DeleteActor(actor *models.Actor) error
}

type actorRepository struct {
	db *gorm.DB
}

func NewActorRepository(db *gorm.DB) ActorRepository {
	return &actorRepository{
		db: db,
	}
}

func (r *actorRepository) GetAll() ([]*models.Actor, error) {
	var actors []*models.Actor
	if err := r.db.Find(&actors).Error; err != nil {
		return nil, err
	}
	return actors, nil
}

func (r *actorRepository) GetAdminById(adminId uint64) (models.Actor, error) {
	admin := models.Actor{}
	err := r.db.Table("actors").Take(&admin, adminId).Error
	return admin, err
}

func (r *actorRepository) GetApprovalByAdminId(adminId uint64) (models.Approval, error) {
	approvals := models.Approval{}
	err := r.db.Table("register_approvals").Where("admin_id = ?", adminId).Take(&approvals).Error
	return approvals, err
}

func (r *actorRepository) UpdateApproval(approval *models.Approval) (*models.Approval, error) {
	var superAdminId *uint
	newSuperAdminId := uint(1)
	superAdminId = &newSuperAdminId

	var status *bool
	newStatus := true
	status = &newStatus

	err := r.db.Table("register_approvals").Model(&approval).Updates(models.Approval{SuperAdminId: superAdminId, Status: *status}).Error
	return approval, err
}

func (r *actorRepository) RegisterActor(actor *models.Actor) (*models.Actor, error) {
	if err := r.db.Create(&actor).Error; err != nil {
		return nil, err
	}
	return actor, nil
}

func (r *actorRepository) RegisterApproval(approval *models.Approval) (*models.Approval, error) {
	if err := r.db.Table("register_approvals").Create(&approval).Error; err != nil {
		return nil, err
	}
	return approval, nil
}

func (r *actorRepository) UpdateAdmin(actor *models.Actor) (*models.Actor, error) {
	err := r.db.Table("actors").Model(&actor).Updates(models.Actor{
		Active:   true,
		Verified: true,
	}).Error
	return actor, err
}
func (r *actorRepository) GetAllApprovalRequest() ([]*models.Approval, error) {
	var approvals []*models.Approval
	err := r.db.Table("register_approvals").Where("super_admin_id IS NULL AND status IS FALSE").Find(&approvals).Error
	return approvals, err
}

func (r *actorRepository) DeleteActor(actor *models.Actor) error {
	err := r.db.Delete(actor).Table("actors").Where("id = ?", actor.Id).Error
	return err
}
