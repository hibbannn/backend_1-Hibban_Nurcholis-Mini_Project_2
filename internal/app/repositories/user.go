package repositories

import (
	"gitlab.com/hibbannn/backend_1-Hibban_Nurcholis-Mini_Project_2/internal/app/modules/user/models"
	"gorm.io/gorm"
)

type UserRepository interface {
	RegisterUser(*models.User) (*models.User, error)
	GetUserByEmail(*models.User) (*models.User, error)
	GetAllUsersByNameAndEmail(name, email string, limit, offset int) ([]*models.User, error)
	SaveUsersIfNotExist(users []*models.User) error
}

type userRepository struct {
	db *gorm.DB
}

func NewUserRepository(db *gorm.DB) UserRepository {
	return &userRepository{
		db: db,
	}
}

func (r *userRepository) RegisterUser(user *models.User) (*models.User, error) {
	if err := r.db.Create(&user).Error; err != nil {
		return nil, err
	}
	return user, nil
}

func (r *userRepository) GetUserByEmail(user *models.User) (*models.User, error) {
	if err := r.db.Where("email = ?", user.Email).First(&user).Error; err != nil {
		return nil, err
	}
	return user, nil
}

func (r *userRepository) GetAllUsersByNameAndEmail(name, email string, limit, offset int) ([]*models.User, error) {
	var users []*models.User
	if err := r.db.Where("CONCAT(first_name, ' ', last_name) LIKE ? AND email LIKE ?", "%"+name+"%", "%"+email+"%").Limit(limit).Offset(offset).Find(&users).Error; err != nil {
		return nil, err
	}
	return users, nil
}

func (r *userRepository) SaveUsersIfNotExist(users []*models.User) error {
	for _, user := range users {
		var count int64
		if err := r.db.Model(&models.User{}).Where("email = ?", user.Email).Count(&count).Error; err != nil {
			return err
		}
		if count == 0 {
			if err := r.db.Create(&user).Error; err != nil {
				return err
			}
		}
	}
	return nil
}
