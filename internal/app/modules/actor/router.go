package actor

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type Router struct {
	handler *RequestHandler
}

func NewRouter(db *gorm.DB) *Router {
	return &Router{
		handler: NewRequestHandler(db),
	}
}

func (r *Router) Handle(e *gin.Engine) {
	basePath := "/actor"
	actors := e.Group(basePath)

	actors.GET("/", r.handler.GetAll)
	actors.POST("/register", r.handler.RegisterActor)
	actors.GET("/approval", r.handler.GetAllApprovalRequest)
	actors.PATCH("/approval/:id", r.handler.RegisterApproval)
	actors.DELETE("/:id", r.handler.DeleteActor)
}
