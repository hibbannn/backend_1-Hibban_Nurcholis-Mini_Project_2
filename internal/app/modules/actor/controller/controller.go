package controller

import (
	"gitlab.com/hibbannn/backend_1-Hibban_Nurcholis-Mini_Project_2/internal/app/dto"
	"gitlab.com/hibbannn/backend_1-Hibban_Nurcholis-Mini_Project_2/internal/app/modules/actor/models"
	"gitlab.com/hibbannn/backend_1-Hibban_Nurcholis-Mini_Project_2/internal/app/modules/actor/usecases"
	"gorm.io/gorm"
)

type Controller interface {
	GetAll() ([]*models.Actor, error)
	RegisterActor(*models.Actor) (any, error)
	GetAllApprovalRequest() ([]*models.Approval, error)
	RegisterApproval(uint64) (any, error)
	DeleteActor(actorId uint64) error
}

type controller struct {
	useCase usecases.UseCase
}

func NewController(db *gorm.DB) Controller {
	return &controller{
		useCase: usecases.NewUseCase(db),
	}
}

func (c *controller) GetAll() ([]*models.Actor, error) {
	actors, err := c.useCase.GetAll()
	var resp []*models.Actor

	for i := 1; i < len(actors); i++ {
		resp = append(resp, &models.Actor{
			Id:        actors[i].Id,
			Username:  actors[i].Username,
			RoleId:    actors[i].RoleId,
			Verified:  actors[i].Verified,
			Active:    actors[i].Active,
			CreatedAt: actors[i].CreatedAt,
			UpdatedAt: actors[i].UpdatedAt,
		})
	}
	return resp, err
}

func (c *controller) RegisterActor(register *models.Actor) (any, error) {
	actor, err := c.useCase.RegisterActor(register)
	if err != nil {
		return nil, err
	}

	res := dto.SuccessCreateActor{
		Response: dto.Response{
			Success:      true,
			MessageTitle: "Success Register Actor",
			Message:      "New Actor Created",
			ResponseTime: "",
		},
		Data: dto.ActorResponse{
			Id:        actor.Id,
			RoleId:    actor.RoleId,
			Verified:  actor.Verified,
			Active:    actor.Active,
			CreatedAt: actor.CreatedAt,
			UpdatedAt: actor.UpdatedAt,
		},
	}
	return res, nil
}

func (c *controller) GetAllApprovalRequest() ([]*models.Approval, error) {
	approvals, err := c.useCase.GetAllApprovalRequest()
	if err != nil {
		return nil, err
	}
	return approvals, nil
}

func (c *controller) RegisterApproval(adminId uint64) (any, error) {
	admin, err := c.useCase.RegisterApproval(adminId)
	if err != nil {
		return nil, err
	}
	return admin, err
}

func (c *controller) DeleteActor(actorId uint64) error {
	err := c.useCase.DeleteActor(actorId)
	if err != nil {
		return err
	}
	return nil
}

//func (uc actorController) RegisterActor(req any) (any, error) {
//	actor, err := uc.actorUsecase.RegisterActor(req.(*entities.Actor))
//	return actor, err
//}
