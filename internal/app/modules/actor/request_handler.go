package actor

import (
	"github.com/gin-gonic/gin"
	ctr "gitlab.com/hibbannn/backend_1-Hibban_Nurcholis-Mini_Project_2/internal/app/modules/actor/controller"
	"gitlab.com/hibbannn/backend_1-Hibban_Nurcholis-Mini_Project_2/internal/app/modules/actor/models"
	"gorm.io/gorm"
	"net/http"
	"strconv"
)

type RequestHandler struct {
	controller ctr.Controller
}

func NewRequestHandler(db *gorm.DB) *RequestHandler {
	return &RequestHandler{
		controller: ctr.NewController(db),
	}
}

func (h *RequestHandler) GetAll(c *gin.Context) {
	actors, err := h.controller.GetAll()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, actors)
}

func (h *RequestHandler) RegisterActor(c *gin.Context) {
	request := &models.Actor{}
	err := c.BindJSON(request)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	actor, err := h.controller.RegisterActor(request)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, actor)
}

func (h *RequestHandler) GetAllApprovalRequest(c *gin.Context) {
	actors, err := h.controller.GetAllApprovalRequest()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, actors)
}

func (h *RequestHandler) RegisterApproval(c *gin.Context) {
	userId, err := strconv.ParseUint(c.Param("id"), 10, 64)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}
	res, err := h.controller.RegisterApproval(userId)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
	}
	c.JSON(http.StatusOK, res)
}

func (h *RequestHandler) DeleteActor(c *gin.Context) {
	actorId, err := strconv.ParseUint(c.Param("id"), 10, 64)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	err = h.controller.DeleteActor(actorId)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"message": "Actor deleted"})
}
