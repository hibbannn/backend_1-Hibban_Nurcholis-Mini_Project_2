package models

import "time"

type Actor struct {
	Id        uint      `gorm:"primary_key"`
	Username  string    `gorm:"column:username"`
	Password  string    `gorm:"column:password"`
	RoleId    uint      `gorm:"column:role_id"`
	Verified  bool      `gorm:"column:verified"`
	Active    bool      `gorm:"column:active"`
	CreatedAt time.Time `gorm:"autoCreateTime"`
	UpdatedAt time.Time `gorm:"autoUpdateTime"`
}
