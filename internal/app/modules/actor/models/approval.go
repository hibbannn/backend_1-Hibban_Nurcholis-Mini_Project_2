package models

import "time"

type Approval struct {
	Id           uint  `gorm:"primary_key"`
	AdminId      uint  `gorm:"column:admin_id"`
	SuperAdminId *uint `gorm:"column:super_admin_id"`
	Status       bool  `gorm:"column:status"`
	CreatedAt    time.Time
	UpdatedAt    time.Time
}
