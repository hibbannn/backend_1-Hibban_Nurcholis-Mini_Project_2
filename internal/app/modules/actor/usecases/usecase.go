package usecases

import (
	"gitlab.com/hibbannn/backend_1-Hibban_Nurcholis-Mini_Project_2/internal/app/modules/actor/models"
	"gitlab.com/hibbannn/backend_1-Hibban_Nurcholis-Mini_Project_2/internal/app/repositories"
	"gorm.io/gorm"
)

type UseCase interface {
	GetAll() ([]*models.Actor, error)
	RegisterActor(*models.Actor) (*models.Actor, error)
	GetAllApprovalRequest() ([]*models.Approval, error)
	RegisterApproval(adminId uint64) (any, error)
	DeleteActor(actorId uint64) error
}

type useCase struct {
	repo repositories.ActorRepository
}

func NewUseCase(db *gorm.DB) UseCase {
	return &useCase{
		repo: repositories.NewActorRepository(db),
	}
}
