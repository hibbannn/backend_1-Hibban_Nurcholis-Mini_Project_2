package usecases

import "gitlab.com/hibbannn/backend_1-Hibban_Nurcholis-Mini_Project_2/internal/app/modules/actor/models"

//func (uc *useCase) Approve(adminId uint) (any, error) {
//	admin, err := uc.accountRepo.GetAdminById(adminId)
//	if err != nil {
//		return nil, err
//	}
//
//	_, err = uc.Repo.UpdateAdmin(&admin)
//	if err != nil {
//		return nil, err
//	}
//
//	approval, err := uc.accountRepo.GetApprovalByAdminId(adminId)
//	if err != nil {
//		return nil, err
//	}
//
//	_, err = uc.accountRepo.UpdateApproval(&approval)
//	if err != nil {
//		return nil, err
//	}
//
//	return admin, err
//}

func (uc *useCase) GetAllApprovalRequest() ([]*models.Approval, error) {
	var model []*models.Approval
	model, err := uc.repo.GetAllApprovalRequest()
	return model, err
}

func (uc *useCase) RegisterApproval(adminId uint64) (any, error) {
	admin, err := uc.repo.GetAdminById(adminId)
	if err != nil {
		return nil, err
	}
	_, err = uc.repo.UpdateAdmin(&admin)
	if err != nil {
		return nil, err
	}
	approval, err := uc.repo.GetApprovalByAdminId(adminId)
	if err != nil {
		return nil, err
	}
	_, err = uc.repo.UpdateApproval(&approval)
	if err != nil {
		return nil, err
	}
	return admin, err
}
