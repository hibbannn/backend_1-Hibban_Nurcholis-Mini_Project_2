package usecases

import (
	"gitlab.com/hibbannn/backend_1-Hibban_Nurcholis-Mini_Project_2/internal/app/modules/actor/models"
	"time"
)

func (uc *useCase) RegisterActor(register *models.Actor) (*models.Actor, error) {
	var actor = &models.Actor{
		Username: register.Username,
		Password: register.Password,
		RoleId:   2,
		Verified: false,
		Active:   false,
	}
	actor, err := uc.repo.RegisterActor(actor)
	if err != nil {
		return nil, err
	}
	var approval = &models.Approval{
		AdminId:      actor.Id,
		SuperAdminId: nil,
		Status:       false,
		CreatedAt:    time.Now(),
		UpdatedAt:    time.Now(),
	}
	approval, err = uc.repo.RegisterApproval(approval)
	if err != nil {
		return nil, err
	}
	return actor, nil
}
