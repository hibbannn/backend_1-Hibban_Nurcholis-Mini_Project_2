package usecases

import "gitlab.com/hibbannn/backend_1-Hibban_Nurcholis-Mini_Project_2/internal/app/modules/actor/models"

func (uc *useCase) GetAll() ([]*models.Actor, error) {
	actors, err := uc.repo.GetAll()
	if err != nil {
		return nil, err
	}
	return actors, nil
}
