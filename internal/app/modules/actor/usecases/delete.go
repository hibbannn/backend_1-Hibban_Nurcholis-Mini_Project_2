package usecases

func (uc *useCase) DeleteActor(actorId uint64) error {
	// Get actor by ID
	actor, err := uc.repo.GetAdminById(actorId)
	if err != nil {
		return err
	}

	// Delete actor from repository
	err = uc.repo.DeleteActor(&actor)
	if err != nil {
		return err
	}

	return nil
}
