package user

import (
	"github.com/gin-gonic/gin"
	ctr "gitlab.com/hibbannn/backend_1-Hibban_Nurcholis-Mini_Project_2/internal/app/modules/user/controller"
	"gitlab.com/hibbannn/backend_1-Hibban_Nurcholis-Mini_Project_2/internal/app/modules/user/models"
	"gorm.io/gorm"
	"net/http"
	"strconv"
)

type RequestHandler struct {
	controller ctr.Controller
}

func NewRequestHandler(db *gorm.DB) *RequestHandler {
	return &RequestHandler{
		controller: ctr.NewController(db),
	}
}

func (h *RequestHandler) RegisterUser(c *gin.Context) {
	request := &models.User{}
	err := c.BindJSON(request)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	user, err := h.controller.RegisterUser(request)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, user)
}

func (h *RequestHandler) GetUserByEmail(c *gin.Context) {
	request := &models.User{}
	err := c.BindJSON(request)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	user, err := h.controller.GetUserByEmail(request)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, user)
}

func (h *RequestHandler) GetAllUsersByNameAndEmail(c *gin.Context) {
	name := c.Query("name")
	email := c.Query("email")
	if name == "" && email == "" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Name or email parameter is required"})
		return
	}
	limit, err := strconv.Atoi(c.DefaultQuery("limit", "10"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid limit parameter"})
		return
	}
	offset, err := strconv.Atoi(c.DefaultQuery("offset", "0"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid offset parameter"})
		return
	}
	users, err := h.controller.GetAllUsersByNameAndEmail(name, email, limit, offset)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, users)
}
