package controller

import (
	"gitlab.com/hibbannn/backend_1-Hibban_Nurcholis-Mini_Project_2/internal/app/modules/user/models"
	"gitlab.com/hibbannn/backend_1-Hibban_Nurcholis-Mini_Project_2/internal/app/modules/user/usecases"
	"gorm.io/gorm"
)

type Controller interface {
	RegisterUser(*models.User) (*models.User, error)
	GetUserByEmail(*models.User) (*models.User, error)
	GetAllUsersByNameAndEmail(name, email string, limit, offset int) ([]*models.User, error)
}

type controller struct {
	useCase usecases.UseCase
}

func NewController(db *gorm.DB) Controller {
	return &controller{
		useCase: usecases.NewUseCase(db),
	}
}

func (c *controller) RegisterUser(user *models.User) (*models.User, error) {
	return c.useCase.RegisterUser(user)
}

func (c *controller) GetUserByEmail(user *models.User) (*models.User, error) {
	return c.useCase.GetUserByEmail(user)
}

func (c *controller) GetAllUsersByNameAndEmail(name, email string, limit, offset int) ([]*models.User, error) {
	users, err := c.useCase.GetAllUsersByNameAndEmail(name, email, limit, offset)
	if err != nil {
		return nil, err
	}
	return users, nil
}
