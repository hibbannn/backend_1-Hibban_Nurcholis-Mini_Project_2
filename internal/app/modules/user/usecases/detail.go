package usecases

import "gitlab.com/hibbannn/backend_1-Hibban_Nurcholis-Mini_Project_2/internal/app/modules/user/models"

//func (uc *useCase) GetUserByEmail(users *models.User) (*models.User, error) {
//	var user = &models.User{
//		Email:     users.Email,
//		//FirstName: users.FirstName,
//		//LastName:  users.LastName,
//		//Avatar:    users.Avatar,
//		//CreatedAt: users.CreatedAt,
//	}
//	return uc.repo.GetUserByEmail(user)
//	if err != nil {
//		return nil, err
//	}
//	return user
//
//}

func (uc *useCase) GetUserByEmail(users *models.User) (*models.User, error) {
	var user = &models.User{
		Email: users.Email,
	}
	user, err := uc.repo.GetUserByEmail(user)
	if err != nil {
		return nil, err
	}
	return user, nil
}
func (uc *useCase) GetAllUsersByNameAndEmail(name, email string, limit, offset int) ([]*models.User, error) {
	users, err := uc.repo.GetAllUsersByNameAndEmail(name, email, limit, offset)
	if err != nil {
		return nil, err
	}
	return users, nil
}
