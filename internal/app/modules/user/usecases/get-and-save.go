package usecases

import (
	"encoding/json"
	"fmt"
	"gitlab.com/hibbannn/backend_1-Hibban_Nurcholis-Mini_Project_2/internal/app/modules/user/models"
	"io/ioutil"
	"net/http"
)

func (uc *useCase) GetAndSaveUsers() error {
	page := 2
	for {
		url := fmt.Sprintf("https://reqres.in/api/users?page=%d", page)
		resp, err := http.Get(url)
		if err != nil {
			return err
		}
		defer resp.Body.Close()

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return err
		}

		var result struct {
			Data []*models.User `json:"data"`
		}
		if err := json.Unmarshal(body, &result); err != nil {
			return err
		}

		if len(result.Data) == 0 {
			break
		}

		if err := uc.repo.SaveUsersIfNotExist(result.Data); err != nil {
			return err
		}

		page++
	}
	return nil
}
