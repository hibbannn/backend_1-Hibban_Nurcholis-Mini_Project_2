package usecases

import "gitlab.com/hibbannn/backend_1-Hibban_Nurcholis-Mini_Project_2/internal/app/modules/user/models"

func (uc *useCase) RegisterUser(register *models.User) (*models.User, error) {
	var user = &models.User{
		FirstName: register.FirstName,
		LastName:  register.LastName,
		Email:     register.Email,
		Avatar:    register.Avatar,
	}
	user, err := uc.repo.RegisterUser(user)
	if err != nil {
		return nil, err
	}
	return user, nil
}
