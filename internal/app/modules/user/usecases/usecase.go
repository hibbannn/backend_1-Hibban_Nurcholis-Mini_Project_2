package usecases

import (
	"gitlab.com/hibbannn/backend_1-Hibban_Nurcholis-Mini_Project_2/internal/app/modules/user/models"
	"gitlab.com/hibbannn/backend_1-Hibban_Nurcholis-Mini_Project_2/internal/app/repositories"
	"gorm.io/gorm"
)

type UseCase interface {
	RegisterUser(*models.User) (*models.User, error)
	GetUserByEmail(*models.User) (*models.User, error)
	GetAllUsersByNameAndEmail(name, email string, limit, offset int) ([]*models.User, error)
	//GetAndSaveUsers(name, email string, limit, offset int) ([]*models.User, error)
}

type useCase struct {
	repo repositories.UserRepository
}

func NewUseCase(db *gorm.DB) UseCase {
	return &useCase{
		repo: repositories.NewUserRepository(db),
	}
}
