package user

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type Router struct {
	handler *RequestHandler
}

func NewRouter(db *gorm.DB) *Router {
	return &Router{
		handler: NewRequestHandler(db),
	}
}

func (r *Router) Handle(e *gin.Engine) {
	basePath := "/user"
	users := e.Group(basePath)

	users.POST("/register", r.handler.RegisterUser)
	users.GET("/:email", r.handler.GetUserByEmail)
	users.GET("/search/", r.handler.GetAllUsersByNameAndEmail)
}
