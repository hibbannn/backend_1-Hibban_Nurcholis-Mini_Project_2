package main

import (
	"fmt"
	"gitlab.com/hibbannn/backend_1-Hibban_Nurcholis-Mini_Project_2/routes"
)

func main() {
	router := routes.Router()
	errRouter := router.Run(":8081")
	if errRouter != nil {
		fmt.Println("Error: running server", errRouter)
		return
	}
}
