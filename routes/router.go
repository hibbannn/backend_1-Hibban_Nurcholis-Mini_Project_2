package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/hibbannn/backend_1-Hibban_Nurcholis-Mini_Project_2/internal/app/modules/actor"
	"gitlab.com/hibbannn/backend_1-Hibban_Nurcholis-Mini_Project_2/internal/app/modules/user"
	"gitlab.com/hibbannn/backend_1-Hibban_Nurcholis-Mini_Project_2/internal/app/utils"
)

func Router() *gin.Engine {
	router := gin.New()
	db := utils.Gorm()
	actorRouter := actor.NewRouter(db)
	userRouter := user.NewRouter(db)
	actorRouter.Handle(router)
	userRouter.Handle(router)
	return router
}
